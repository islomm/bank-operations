import CreateCustomer from "./CreateCustomer";
import { useSelector } from "react-redux";
import Customer from "./Customer";
import AccountOperations from "./AccountOperations";
import BalanceDisplay from "./BalanceDisplay";

function App() {
  const { name } = useSelector((state) => state.customer);
  return (
    <div className="container">
      {name == "" ? (
        <CreateCustomer />
      ) : (
        <>
          <Customer />
          <AccountOperations />
          <BalanceDisplay />
        </>
      )}
    </div>
  );
}

export default App;
