import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { createCustomer } from "./features/customerSlice";

function CreateCustomer() {
  const [fullName, setFullName] = useState("");
  const [nationalId, setNationalId] = useState("");
  const dispatch = useDispatch();
  function handleClick() {
    if (!fullName || !nationalId) return;
    dispatch(
      createCustomer({
        name: fullName,
        nationalId: nationalId,
        createdAt: new Date().toISOString(),
      })
    );
  }
  return (
    <div>
      <h2>Create new customer</h2>
      <div className="w-50 border inputs">
        <div>
          <label>Customer full name : </label>
          <input
            value={fullName}
            onChange={(e) => setFullName(e.target.value)}
          />
        </div>
        <div>
          <label>National ID : </label>
          <input
            value={nationalId}
            onChange={(e) => setNationalId(e.target.value)}
          />
        </div>
        <button className="btn btn-success" onClick={handleClick}>
          Create new customer
        </button>
      </div>
    </div>
  );
}

export default CreateCustomer;
