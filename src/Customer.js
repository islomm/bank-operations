import { useSelector } from "react-redux";

function Customer() {
  const { name } = useSelector((state) => state.customer);
  return <h2>👋 Welcome {name}</h2>;
}

export default Customer;
