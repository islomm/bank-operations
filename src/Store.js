import { configureStore } from "@reduxjs/toolkit";
import customerSlice from "./features/customerSlice";
import accountSlice from "./features/accountSlice";

const store = configureStore({
  reducer: {
    customer: customerSlice,
    account: accountSlice,
  },
});

export default store;
