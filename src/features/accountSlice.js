const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  balance: 0,
  loan: 0,
  loanPurpose: "",
};

const accountSlice = createSlice({
  name: "account",
  initialState,
  reducers: {
    deposit(state, action) {
      state.balance += action.payload;
    },
    withdraw(state, action) {
      state.balance -= action.payload;
    },
    requestLoan(state, action) {
      if (state.loan > 0) alert("You can not request loan");
      state.balance = state.balance + action.payload.amount;
      state.loan = action.payload.amount;
      state.loanPurpose = action.payload.purpose;
    },
    payLoan(state, action) {
      state.balance = state.balance - state.loan;
      state.loan = 0;
      state.loanPurpose = "";
    },
  },
});

export const { withdraw, requestLoan, payLoan } = accountSlice.actions;

export default accountSlice.reducer;

export function deposit(depositAmount, currency) {
  console.log(depositAmount);
  console.log(currency);
  if (currency == "USD") return accountSlice.actions.deposit(depositAmount);
  return (dispatch, getState) => {
    fetch(
      `https://api.frankfurter.app/latest?amount=${depositAmount}&from=${currency}&to=USD`
    )
      .then((resp) => resp.json())
      .then((data) => {
        console.log(data);
        return dispatch(accountSlice.actions.deposit(data.rates.USD));
      });
  };
}
