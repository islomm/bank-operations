import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  name: "",
  nationalId: 0,
  createdAt: 0,
};

export const customerSlice = createSlice({
  name: "customer",
  initialState,
  reducers: {
    createCustomer: (state, action) => {
      state.name = action.payload.name;
      state.nationalId = action.payload.nationalId;
      state.createdAt = new Date().toISOString();
    },
  },
});

export const { createCustomer } = customerSlice.actions;

export default customerSlice.reducer;
